import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TicketFormService } from './ticket-form.service';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.scss']
})
export class TicketFormComponent implements OnInit {
  fileToUpload: File = null;
  complain: FormGroup;

  constructor(private fb: FormBuilder, private ticketForm: TicketFormService) { }

  ngOnInit() {

    this.complain = this.fb.group({
      from: [''],
      category: [''],
      facility: [''],
      priority: [''],
      vendors: [''],
      description: ['']

    });

  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
}

onSubmit( {value} ) {
  console.log('values', value);

  this.ticketForm.addComplain(value).subscribe( response => {
    // const res = response['complain'];
     console.log('response', response);

  },
  err => {
    console.log(err);
  }
  );
}

}
