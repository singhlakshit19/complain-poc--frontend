import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class TicketFormService {
baseUrl= environment.baseUrl;
  constructor(private http: HttpClient) { }

  addComplain(complain) {

    const url = `${this.baseUrl}/complain/add-complain`;
    return this.http.post(url, complain);
  }

}
